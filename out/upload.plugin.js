var __hasProp = {}.hasOwnProperty;
var __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

var handler = require('./handler');

module.exports = function(BasePlugin) {
    var UploadPlugin;
    return UploadPlugin = (function(_super) {

        var docpadConfig = docpad.getConfig();

        __extends(UploadPlugin, _super);

        function UploadPlugin() {
            return UploadPlugin.__super__.constructor.apply(this, arguments);
        }

        UploadPlugin.prototype.name = 'upload';

        UploadPlugin.prototype.config = (function(config) {
            config = config || {};
            return {
                path: config.path || 'upload'
            }
        })(docpadConfig.plugins.backbonecms);

        UploadPlugin.prototype.serverExtend = function(opts) {
            var me = this;

            var app = opts.serverExpress;

            console.log(1111);
            for(var k in opts) {
                console.log(k);
            }

            var upload = require('jquery-file-upload-middleware');

//            app.use('/n' + me.config.path, upload.fileHandler({
//                tmpDir: __dirname + '/tmp',
//                uploadDir: process.cwd() + '/' + docpadConfig.outPath + '/' + me.config.path,
//                uploadUrl: '/' + me.config.path
//            }));
        };

        return UploadPlugin;

    })(BasePlugin);
};
