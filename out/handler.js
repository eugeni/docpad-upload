'use strict';
var path = require('path'),
    fs = require('fs'),
// Since Node 0.8, .existsSync() moved from path to fs:
    _existsSync = fs.existsSync || path.existsSync,
    formidable = require('formidable'),
    nodeStatic = require('node-static'),
    options = {
        tmpDir: __dirname + '/tmp',
        publicDir: __dirname + '/public',
        uploadDir: __dirname + '/public/files',
        uploadUrl: '/files/',
        maxPostSize: 11000000000, // 11 GB
        minFileSize: 1,
        maxFileSize: 10000000000, // 10 GB
        acceptFileTypes: /.+/i,
        // Files not matched by this regular expression force a download dialog,
        // to prevent executing any scripts in the context of the service domain:
        inlineFileTypes: /\.(gif|jpe?g|png)$/i,
        accessControl: {
            allowOrigin: '*',
            allowMethods: 'OPTIONS, HEAD, GET, POST, PUT, DELETE',
            allowHeaders: 'Content-Type, Content-Range, Content-Disposition'
        },
        /* Uncomment and edit this section to provide the service via HTTPS:
         ssl: {
         key: fs.readFileSync('/Applications/XAMPP/etc/ssl.key/server.key'),
         cert: fs.readFileSync('/Applications/XAMPP/etc/ssl.crt/server.crt')
         },
         */
        nodeStatic: {
            cache: 3600 // seconds to cache served files
        }
    },
    utf8encode = function(str) {
        return unescape(encodeURIComponent(str));
    },
    fileServer = new nodeStatic.Server(options.publicDir, options.nodeStatic),
    nameCountRegexp = /(?:(?: \(([\d]+)\))?(\.[^.]+))?$/,
    nameCountFunc = function(s, index, ext) {
        return ' (' + ((parseInt(index, 10) || 0) + 1) + ')' + (ext || '');
    },
    FileInfo = function(file) {
        this.name = file.name;
        this.size = file.size;
        this.type = file.type;
        this.deleteType = 'DELETE';
    },
    UploadHandler = function(req, res, callback) {
        this.req = req;
        this.res = res;
        this.callback = callback;
    },
    serve = function(req, res) {
        console.log('handler request received');
        console.log('handler: method', req.method);
        res.setHeader(
            'Access-Control-Allow-Origin',
            options.accessControl.allowOrigin
        );
        res.setHeader(
            'Access-Control-Allow-Methods',
            options.accessControl.allowMethods
        );
        res.setHeader(
            'Access-Control-Allow-Headers',
            options.accessControl.allowHeaders
        );
        var handleResult = function(result, redirect) {
                if (redirect) {
                    res.writeHead(302, {
                        'Location': redirect.replace(
                            /%s/,
                            encodeURIComponent(JSON.stringify(result))
                        )
                    });
                    res.end();
                }
                else {
                    res.writeHead(200, {
                        'Content-Type': req.headers.accept
                            .indexOf('application/json') !== -1 ?
                            'application/json' : 'text/plain'
                    });
                    res.end(JSON.stringify(result));
                }
            },
            setNoCacheHeaders = function() {
                res.setHeader('Pragma', 'no-cache');
                res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate');
                res.setHeader('Content-Disposition', 'inline; filename="files.json"');
            },
            handler = new UploadHandler(req, res, handleResult);

        switch (req.method) {
            case 'OPTIONS':
                res.end();
                break;
            case 'HEAD':
            case 'GET':
                if (req.url === '/') {
                    setNoCacheHeaders();
                    if (req.method === 'GET') {
                        //handler.get();
                        res.end("No listing");
                    }
                    else {
                        res.end();
                    }
                }
                else {
                    req.url = decodeURIComponent(req.url); // HACK: decodeURI in node-static does not decode names like /files/book1-%2B2.txt
                    fileServer.serve(req, res, function(err, result) {
                        if (err) {
                            // Respond to the client
                            var msg = "Error " + err.status + " - " + err.message;
                            console.error(msg + " URL: " + req.url);
                            res.writeHead(err.status, err.headers);
                            res.write(msg);
                            res.end();
                        }
                    });
                }
                break;
            case 'POST':
                setNoCacheHeaders();
                handler.post();
                break;
            case 'DELETE':
                handler.destroy();
                break;
            default:
                res.statusCode = 405;
                res.end();
        }
    };
fileServer.respond = function(pathname, status, _headers, files, stat, req, res, finish) {
    // Prevent browsers from MIME-sniffing the content-type:
    _headers['X-Content-Type-Options'] = 'nosniff';
    if (!options.inlineFileTypes.test(files[0])) {
        // Force a download dialog for unsafe file extensions:
        _headers['Content-Type'] = 'application/octet-stream';
        _headers['Content-Disposition'] = 'attachment; filename="' +
            utf8encode(path.basename(files[0])) + '"';
    }
    nodeStatic.Server.prototype.respond
        .call(this, pathname, status, _headers, files, stat, req, res, finish);
};
FileInfo.prototype.validate = function() {
    if (options.minFileSize && options.minFileSize > this.size) {
        this.error = 'File is too small';
    }
    else
    if (options.maxFileSize && options.maxFileSize < this.size) {
        this.error = 'File is too big';
    }
    else
    if (!options.acceptFileTypes.test(this.name)) {
        this.error = 'Filetype not allowed';
    }
    return !this.error;
};
FileInfo.prototype.safeName = function(subdir) {
    // Prevent directory traversal and creating hidden system files:
    this.name = path.basename(this.name).replace(/^\.+/, '');
    // Replacing some characters illegal on different OSs (such as ':' on Windows)
    this.name = path.basename(this.name).replace(/[\\\/:]/, '-');
    // Prevent overwriting existing files:
    while (_existsSync(options.uploadDir + subdir + '/' + this.name)) {
        this.name = this.name.replace(nameCountRegexp, nameCountFunc);
    }
};
FileInfo.prototype.initUrls = function(req) {
    if (!this.error) {
        var that = this,
            baseUrl = (options.ssl ? 'https:' : 'http:') +
                '//' + req.headers.host + options.uploadUrl;
        this.url = this.deleteUrl = baseUrl + req.url.substr(1) + '/' + encodeURIComponent(this.name);
    }
};
UploadHandler.prototype.get = function() {
    var handler = this,
        files = [];
    fs.readdir(options.uploadDir, function(err, list) {
        list.forEach(function(name) {
            var stats = fs.statSync(options.uploadDir + '/' + name),
                fileInfo;
            if (stats.isFile() && name[0] !== '.') {
                fileInfo = new FileInfo({
                    name: name,
                    size: stats.size
                });
                fileInfo.initUrls(handler.req);
                files.push(fileInfo);
            }
        });
        handler.callback({files: files});
    });
};
UploadHandler.prototype.post = function() {
    console.log('handler: post handler');
    var handler = this,
        form = new formidable.IncomingForm(),
        tmpFiles = [],
        files = [],
        map = {},
        counter = 1,
        redirect,
        finish = function() {
            counter -= 1;
            if (!counter) {
                files.forEach(function(fileInfo) {
                    fileInfo.initUrls(handler.req);
                });
                handler.callback({files: files}, redirect);
            }
        };

    form.uploadDir = options.tmpDir;

    form.on('fileBegin',function(name, file) {
        console.log('handler: on fileBegin', form.uploadDir);
        tmpFiles.push(file.path);
        var fileInfo = new FileInfo(file, handler.req, true);
        fileInfo.safeName(handler.req.url);
        map[path.basename(file.path)] = fileInfo;
        files.push(fileInfo);
    }).on('field',function(name, value) {
            console.log('handler: on field', name);
            if (name === 'redirect') {
                redirect = value;
            }
        }).on('file',function(name, file) {
            console.log('handler: on file', name);
            var fileInfo = map[path.basename(file.path)];
            fileInfo.size = file.size;
            if (!fileInfo.validate()) {
                fs.unlink(file.path);
                return;
            }

            try {
                var upl = options.uploadDir + handler.req.url;
                try {
                    fs.mkdirSync(upl);
                }
                catch (err) {
                    if (err.code !== "EEXIST")
                        throw err;
                }
                fs.renameSync(file.path, upl + '/' + fileInfo.name);
            }
            catch (err) {
                console.error("Error: " + err.message);

                var errMsg = err.message;

                if (err.code === "ENAMETOOLONG") {
                    errMsg = "Имя файла слишком длинное для сохранения на сервере. Оно не должно превышать 125 символов.";
                }

                this.error = err.message;
                handler.res.writeHead(500, {
                    'Content-Type': handler.req.headers.accept
                        .indexOf('application/json') !== -1 ?
                        'application/json' : 'text/plain'
                });
                handler.res.end(errMsg);
            }
        }).on('aborted',function() {
            console.log('handler: on aborted');
            tmpFiles.forEach(function(file) {
                fs.unlink(file);
            });
        }).on('error',function(e) {
            console.log("POST error: " + e);
        }).on('progress',function(bytesReceived, bytesExpected) {
            console.log('handler: on progress', bytesReceived);
            if (bytesReceived > options.maxPostSize) {
                handler.req.connection.destroy();
            }
        }).on('end', finish).parse(handler.req);
};
UploadHandler.prototype.destroy = function() {
    var handler = this,
        fileName;

    var uri = decodeURIComponent(handler.req.url);
    if (handler.req.url.slice(0, options.uploadUrl.length) === options.uploadUrl) {
        fileName = path.basename(uri);
        if (fileName[0] !== '.') {
            var fullPath = path.join(path.dirname(options.uploadDir), path.dirname(uri), fileName);

            fs.unlink(fullPath, function(err) {
                if (err) {
                    console.error(err);
                    handler.callback({success: false, error: err});
                }
                else
                    handler.callback({success: true});
            });
            return;
        }
    }
    else { // delete the entire directory request arrived
        if (path.basename(uri) === '*' && handler.req.url.indexOf('.') === -1) {
            var rimraf = require('rimraf');
            var fullPath = path.join(options.uploadDir, path.dirname(uri));
            rimraf(fullPath, function(err) {
                if (err) {
                    console.error(err);
                    handler.callback({success: false, error: err});
                }
                else
                    handler.callback({success: true});
            });
        }
        return;
    }

    handler.callback({success: false});
};

module.exports = serve;
